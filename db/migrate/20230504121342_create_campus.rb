class CreateCampus < ActiveRecord::Migration[6.0]
  def change
    create_table :campus do |t|
      t.string :nome
      t.text :endereco
      t.string :numero
      t.string :bairro
      t.string :cidade
      t.string :cep

      t.timestamps
    end
  end
end
