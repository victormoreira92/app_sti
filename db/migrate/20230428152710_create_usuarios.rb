class CreateUsuarios < ActiveRecord::Migration[6.0]
  def change
    create_table :usuarios do |t|
      t.boolean :admin
      t.datetime :data_desativacao
      t.timestamps
    end
  end
end
