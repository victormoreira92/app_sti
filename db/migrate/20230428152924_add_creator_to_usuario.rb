class AddCreatorToUsuario < ActiveRecord::Migration[6.0]
  def change
    add_column :usuarios, :creator_id, :integer
    add_index :usuarios, :creator_id, unique: true

  end
end
