class CreateCaronas < ActiveRecord::Migration[6.0]
  def change
    create_table :caronas do |t|
      t.datetime :quando
      t.decimal :valor, precision: 5, scale: 2
      t.integer :passageiros
      t.text :observacao
      t.string :partida
      t.string :destino
      t.belongs_to :usuario, null: false, foreign_key: true

      t.timestamps
    end
  end
end
