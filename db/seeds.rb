# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)




9.times{ |i|
    Campu.find_or_create_by!(nome: "Campus-#{i +1}", endereco: Faker::Address.street_name, numero: rand(10), bairro: Faker::Address.community, cidade: Pub::Cidade.where("estado_id = '18'").all.sample.nome, cep: "#{Faker::Number.number(digits: 5)}#{Faker::Number.number(digits: 3)}")
    puts Campu.last
}


30.times{ |i|
    if i > 2
        Usuario.find_or_create_by!(admin: [true, false, false, false].sample, data_desativacao: Time.now, creator_id: i)
    end
}




70.times{ |i|
    if i < 5 
        Carona.find_or_create_by!(quando: Time.now, valor: Faker::Number.decimal(l_digits: 2), passageiros: rand(0..5), observacao: Faker::Lorem.sentence(word_count: 3), partida: Faker::Address.street_name, destino: Campu.all.sample.nome, usuario: Usuario.all.sample)
    elsif i > 5 && i < 10
        Carona.find_or_create_by!(quando: Time.now, valor: Faker::Number.decimal(l_digits: 2), passageiros: rand(0..5), observacao:"", partida: Faker::Address.street_name, destino: Campu.all.sample.nome, usuario: Usuario.second)
    else
        Carona.find_or_create_by!(quando: Time.now, valor: Faker::Number.decimal(l_digits: 2), passageiros: rand(0..5), observacao: Faker::Lorem.sentence(word_count: 3), partida: Faker::Address.street_name, destino: Campu.all.sample.nome, usuario: Usuario.all.sample)
    end
    
}

90.times{
    Parada.find_or_create_by!(paradas: Faker::Address.street_name, carona: Carona.all.sample)
}