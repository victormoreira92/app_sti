class Carona < ApplicationRecord
  belongs_to :usuario
  has_many :paradas, inverse_of: :carona
  accepts_nested_attributes_for :paradas, allow_destroy: true 

  #Validacao de carona para sempre ter ou uma partida ou destino para a UFF
  validate :validar_partida_destino

  def validar_partida_destino
    unless Campu.all.pluck(:nome).include?(partida) || Campu.all.pluck(:nome).include?(destino)
      errors.add(:partida, "deve ser um dos campus")
      errors.add(:destino, "deve ser um dos campus")
    end
  end

  validates :valor, numericality: { greater_than: 0, less_than: 1000 }
  validates :observacao, length: {maximum: 255}, allow_blank: true
  validates :destino, presence: :true, length: {minimum:3, maximum:55}
  validates :partida, presence: :true, length: {minimum:3, maximum:55}
  validates :passageiros, numericality: {greater_than:0, less_than: 5}
end
