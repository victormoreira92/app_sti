class Usuario < ApplicationRecord


    belongs_to :creator, class_name: "Pub::IdentificacaoLogin", foreign_key: "creator_id"
    has_many :caronas
    
    validates_associated :creator,:message => "Nome não deve estar vazio", :if => Proc.new {|usuario|usuario.creator.nome.blank?}
    validates_associated :creator,:message => "IDUFF não deve estar vazio", :if => Proc.new {|usuario|usuario.creator.iduff.blank?}
    
    validates_with UsuarioValidacao


    def toogle_data_desativacao
        if self.data_desativacao.nil?
            self.data_desativacao = Time.now()
        else
            self.data_desativacao = nil
        end
    end

    



    def encontrar_iduff(iduff)
        Pub::IdentificacaoLogin.find_by(iduff: iduff).present?
    end
    
    private 
        

end
