class Campu < ApplicationRecord
    validates_length_of :nome, within: 7..25, presence: true,message: "numero de caracteres invalido"

    validates_length_of :endereco, within: 15..100, presence: true, message: "numero de caracteres invalido"

    validates_length_of :bairro, minimum: 2, message: "numero de abaixo do permitido "

    validates_length_of :cidade, minimum: 2, message: "numero de abaixo do  permitdo"


    validates_length_of :cep, is: 8, presence: true, message: "deve possuir 8 caracteres"
end
