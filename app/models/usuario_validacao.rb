class UsuarioValidacao < ActiveModel::Validator

    def validate(record)
        id_usuario = Pub::IdentificacaoLogin.find_by(iduff:record.creator.iduff)
        if id_usuario
            record.creator = id_usuario            
        else
            record.errors.add :IDUFF, "não existe"
        end
    end

end