# frozen_string_literal: true

class Ability
  include CanCan::Ability


  def initialize(usuario)
    user ||= Usuario.all.detect{|usu| usu.creator.iduff == usuario.iduff}

    if user.admin?
      can :manage, :all
    else
      can :read, Carona
      can [:create, :update], Carona, usuario: user
    end
  end
end
