// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("jquery")
require("@nathanvda/cocoon")
require('select2')
require("channels")


// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)


import "bootstrap"
import select2 from 'select2';
import 'select2/dist/css/select2.css';
import Inputmask from 'inputmask';
import '../funcoesjs/maskinput.js'

window.$ = $


$(document).ready(function(){
  $('.select2-caronas').value = "";

  $('.select2-usuarios').select2({
    placeholder: 'Selecione um usuario',
    allowClear: true,
    ajax:{
      url:"/usuarios/encontrar",
      dataType: "json",
      delay: 1000,
      data: function(params) {
        return { usuario: params.term };
      },
      processResults: function (data, params) {
        return {
          results: $.map(data, function(value, index){
            return {id: value.id, text: value.creator.iduff+"-"+value.creator.nome};
          })
        };
      }
    }
  })

  $('.select2-caronas').select2({
    placeholder: 'Digite o bairro da carona...',
    allowClear: true,
    ajax:{
      url:"/caronas/encontrarcaronas",
      dataType: "json",
      delay: 1000,
      data: function(params) {
        return { bairro: params.term };
      },
      processResults: function (data, params) {
        return {
          results: $.map(data, function(value, index){
            return {id: value.id, text: value.partida+" - "+value.destino};
          })
        };
      }
    }
  })
});

$(document).on('change', '.select2-caronas', function(){
  var caronaId = $(this).val();
  window.location.href = "caronas/"+caronaId
});

$(document).on('change', '.select2-usuarios', function(){
  var usuarioId = $(this).val();
  window.location.href = "usuarios/"+usuarioId
});

$(document).ready(function(){
  $( ".click:first" ).on( "click", function() {
    $(".campus-lista:first").toggleClass( "d-none" );
  } );

  $( ".click:last" ).on( "click", function() {
    $(".campus-lista:last").toggleClass( "d-none" );
  } );

  $(".nome-campus-destino").on("click", function(){
    $(".destino").val(this.textContent);
  })

  $(".nome-campus-partida").on("click", function(){
    $(".partida").val(this.textContent);
  })

})
