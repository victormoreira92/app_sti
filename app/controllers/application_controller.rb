class ApplicationController < ActionController::Base
  
    def usuario_adm
        creator_id = Pub::IdentificacaoLogin.where(iduff: current_user.iduff).first.id
        admin = Usuario.find(creator_id).admin?        
        @usuario_admin ||= admin
         
    end

    rescue_from CanCan::AccessDenied do |exception|
      respond_to do |format|
        format.html { render :file => "#{Rails.root}/public/401", :layout => false, :status => :not_found }      
      end
    end
    
end
