class CaronasController < ApplicationController
  authenticate_with_iduff_keycloak
  load_and_authorize_resource

  before_action :set_carona, only: %i[ show edit update destroy ]

  # GET /caronas or /caronas.json
  def index
    if params[:usuario].present?
      usuario_id = Pub::IdentificacaoLogin.find_by(iduff: params[:usuario]).id
      carona_usuario = Usuario.find(usuario_id)
      @caronas = carona_usuario.caronas.order(quando: :desc).all
    else
      @caronas = Carona.order(quando: :desc).all
    end 


  end

  # GET /caronas/1 or /caronas/1.json
  def show
  end

  # GET /caronas/encontrar
  # POST /caronas/encontrar
  def encontrarcaronas
    if params[:bairro].present?
      carona_por_nome = Carona.where("partida LIKE ? OR destino LIKE ?","%#{params[:bairro]}%","%#{params[:bairro]}%")                            
      render json: carona_por_nome.map{|v| v.serializable_hash(only: [:id, :partida, :destino])}
      
    else
      render json: []
    end
  end

  

  # GET /caronas/new
  def new
    @carona = Carona.new()
    @campus = Campu.all
  end

  # GET /caronas/1/edit
  def edit
    @campus = Campu.all
  end

  # POST /caronas or /caronas.json
  def create
    @carona = Carona.new(carona_params)    
    @carona.usuario_id = encontrar_id_current_user

    respond_to do |format|
      if @carona.save
        format.html { redirect_to carona_url(@carona), notice: "Carona was successfully created." }
        format.json { render :show, status: :created, location: @carona }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @carona.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /caronas/1 or /caronas/1.json
  def update
      respond_to do |format|
        if @carona.update(carona_params)
          format.html { redirect_to carona_url(@carona), notice: "Carona was successfully updated." }
          format.json { render :show, status: :ok, location: @carona }
        else
          format.html { render :edit, status: :unprocessable_entity }
          format.json { render json: @carona.errors, status: :unprocessable_entity }
        end
      end
      @campus = Campu.all
  end

  # DELETE /caronas/1 or /caronas/1.json
  def destroy
    @carona.destroy

    respond_to do |format|
      format.html { redirect_to caronas_url, notice: "Carona was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_carona
      @carona = Carona.find(params[:id])
    end

    #Encontrar o id do usuario atual e retorna o id deste usuario
    def encontrar_id_current_user 
      Pub::IdentificacaoLogin.where("iduff LIKE ?", "%#{current_user.iduff}%").pluck(:id).first
    end
    #Verificar se corrida é do usuario logado
    
    # Only allow a list of trusted parameters through.
    def carona_params
      params.require(:carona).permit(:quando, :valor, :passageiros, :observacao, :partida, :destino, :usuario_id, paradas_attributes:[:id, :paradas])
    end
end
