class UsuariosController < ApplicationController
  authenticate_with_iduff_keycloak
  load_and_authorize_resource
  before_action :set_usuario, only: %i[ show edit update destroy ]

  # GET /usuarios or /usuarios.json
  def index
    @usuarios = Usuario.all
    @nomes_finder = Pub::IdentificacaoLogin.all

  end

  # GET /usuarios/1 or /usuarios/1.json
  def show
        
  end
  
  # GET /usuarios/encontrar
  # POST /usuarios/encontrar
  def encontrar
    if params[:usuario].present?
      ids_usuarios_encontrados = Pub::IdentificacaoLogin.where("nome LIKE ? OR iduff LIKE ?","%#{params[:usuario]}%","%#{params[:usuario]}%").pluck(:id)                
      usuarios_encontrados = Usuario.where(creator_id: ids_usuarios_encontrados)                              
      
      render json: usuarios_encontrados.map{|v| v.serializable_hash(include: {creator: {only: [:iduff,:nome]}}, only: :id)}
    else 
      render json: []
    end
  end

  # GET /usuarios/new
  def new
    @usuario = Usuario.new
  end

  # post /usuarios/toogleusuario
  def toogleusuario
    if params[:id].present?
      usuario_toogle = Usuario.find(params[:id])
      usuario_toogle.toogle_data_desativacao
      usuario_toogle.save!

      respond_to do |format|
        if usuario_toogle.data_desativacao.nil?
          format.html {redirect_to usuarios_path, notice: "Usuario #{usuario_toogle.creator.nome} foi desativado" }
        else
          format.html {redirect_to usuarios_path, notice: "Usuario #{usuario_toogle.creator.nome} foi ativado" }
        end
      end
    end

    
    
  end

  # GET /usuarios/1/edit
  def edit
    @usario_creator = @usuario.creator
  end

  # POST /usuarios or /usuarios.json
  def create
    @usuario = salvar_usuario
    
    respond_to do |format|
      if @usuario.save       
        format.html { redirect_to usuario_url(@usuario), notice: "Usuario was successfully created." }
        format.json { render :show, status: :created, location: @usuario }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /usuarios/1 or /usuarios/1.json
  def update
    respond_to do |format|

      if @usuario.update(usuario_params) 
        format.html { redirect_to usuario_url(@usuario), notice: "Usuario was successfully updated." }
        format.json { render :show, status: :ok, location: @usuario }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /usuarios/1 or /usuarios/1.json
  def destroy
    @usuario.destroy
    @usuario.creator.destroy

    respond_to do |format|
      format.html { redirect_to usuarios_url, notice: "Usuario was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_usuario
      @usuario = Usuario.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def usuario_params
      params.require(:usuario).permit(:admin, :nome, :iduff, :data_desativacao)
    end
    
    def encontrar_usuario
      if params[:nome].present? && !params[:nome].nil?
        nomes_encontrados = Pub::IdentificacaoLogin.where("nome LIKE ?", "%#{params[:nome]}%").pluck(:id)                
        usuarios = Usuario.where(creator_id: nomes_encontrados)
        usuarios.all        
      else
        Usuario.all      
      end      
            
    end

    def salvar_usuario
        usuario = Usuario.new()
        usuario.admin = params[:usuario][:admin]
        usuario.creator = Pub::IdentificacaoLogin.new(iduff: params[:creator][:iduff], nome: params[:creator][:nome])
        usuario.data_desativacao = params[:usuario][:data_desativacao]
        usuario
    end

end
