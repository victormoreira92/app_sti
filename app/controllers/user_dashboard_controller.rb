class UserDashboardController < ApplicationController
  authenticate_with_iduff_keycloak
  
  def index
    usuario_id = Pub::IdentificacaoLogin.find_by(iduff: current_user.iduff).id
    carona_usuario = Usuario.find(usuario_id)
    @caronas_usuario = carona_usuario.caronas.order(quando: :desc).all
    session[:usuarioadm] = usuario_adm

  end
  
end
