class WelcomeController < ApplicationController

  def index
    @campus = Campu.all
    @usuarios = Usuario.all
    @caronas = Carona.all
    @paradas = Parada.all
  end

  def redirecionarusuario
    respond_to do |format|
      format.html {redirect_to user_dashboard_index_path}
    end
  end 
end
