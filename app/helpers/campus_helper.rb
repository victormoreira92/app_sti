module CampusHelper
    
    #Criar lista de listas do modelo Pub::Cidade para criar option do select
    def criar_options (modelo_lista)
        lista_option = []
        modelo_lista.each do |obj|
            lista_option << [obj.nome, obj.nome]
        end
        return lista_option
    end

    def cep_mask(atributo)
        "#{atributo[0..4]}-#{atributo[5..7]}"
    end
end
