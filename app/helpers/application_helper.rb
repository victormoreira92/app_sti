module ApplicationHelper
    def show_area(iduff)
     creator_id = Pub::IdentificacaoLogin.where(iduff: iduff)[0].id
     admin = Usuario.where(creator_id: creator_id)[0].admin
        if admin
            return "Admistrador"
        else
            return "Usuario"
        end
    end

    def mostrar_nome_usuario(iduff)
        Pub::IdentificacaoLogin.where(iduff: iduff)[1].nome
    end
    
    def usuario_is_admin?
        Usuario.find(Pub::IdentificacaoLogin.find_by(iduff: current_user.iduff).id).admin
    end
end
