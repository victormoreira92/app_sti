module UsuariosHelper
    def usuario_desativado(atributo)
        return "bg-secondary text-white" unless atributo.nil?
    end

    def usuario_nome_botao(atributo)
        atributo.nil? ? "Desativar usuario" : "Ativar usuario"
    end

    def usuario_verificar_data_desativacao(atributo)
        atributo.nil? ? "Ativo" : "Desativado"
    end

    def usuario_is_admin(atributo)
        atributo ? "Administrador" : "Usuário"
    end

    def usuario_iduff_mask(atributo)
        "#{atributo[0..2]}.#{atributo[3..5]}.#{atributo[6..8]}-#{atributo[9..10]}"
    end
end
