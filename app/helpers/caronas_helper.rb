module CaronasHelper
    def carona_pertence_ao_usuario?
        carona_usuario = Carona.find(params[:id]).usuario
        carona_usuario.creator.iduff == current_user.iduff
    end
end
