json.extract! campu, :id, :nome, :endereco, :numero, :bairro, :cidade, :cep, :created_at, :updated_at
json.url campu_url(campu, format: :json)
