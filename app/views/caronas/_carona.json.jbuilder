json.extract! carona, :id, :quando, :valor, :passageiros, :observacao, :partida, :destino, :usuario_id, :created_at, :updated_at
json.url carona_url(carona, format: :json)
