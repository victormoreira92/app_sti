Rails.application.routes.draw do
  get 'user_dashboard/index'
  get 'adm_dashboard/index'

  post 'usuarios/encontrar'
  get 'usuarios/encontrar'

  post 'welcome/redirecionarusuario'
  
  get 'caronas/encontrarcaronas'
  post 'usuarios/toogleusuario'

  resources :campus
  resources :usuarios
  resources :caronas


  resources :caronas do
    member do
      get :encontrarcaronas
    end
  end
  
  get 'welcome/index'
  root 'welcome#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
