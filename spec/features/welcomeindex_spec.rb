require 'rails_helper'

RSpec.feature "Welcome index", type: :feature do
    before(:example) do
        visit(welcome_index_path)
    end
    describe "#cabecalho" do
        context "links presentes" do
            it "link Numeros" do
                expect(page).to have_content("Números")
            end

            it "link Numeros" do
                expect(page).to have_content("Como funciona")
            end

            it "link Numeros" do
                expect(page).to have_content("Campus atendidos")
            end

            it "btn Entrar" do
                expect(page).to have_button("Entrar")
            end
        end
    end

    describe "#click" do
        context "Btn Entrar" do
            it "click no btn Entrar" do
                click_on('Entrar')
                expect(page).to have_content("UFF")
            end
        end
    end
  
  
end