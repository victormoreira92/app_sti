require 'rails_helper'

RSpec.describe Usuario, type: :model do
  let(:usuario){create(:usuario)}

  describe "#creator" do
    context "Presença de creator" do
      it "Usuario valido com creator" do
        expect(usuario).to be_valid
      end
    end
  end
  
  describe "creator#iduff" do
    context "Existencia de iduff" do

      it "Usuario não deve ser valido sem iduff" do
        creator = Pub::IdentificacaoLogin.new(nome:"Daniel", iduff: nil)
        creator.save(validate: false)
        usuario_invalido = Usuario.create(admin: true, creator: creator)
        expect(usuario_invalido).not_to be_valid
      end

      it "Usuario ser valido com iduff presente" do
        expect(usuario).to be_valid
      end
    end
    
    # context "Tamnho do iduff" do
    #   let(:iduff_menor){create(:usuario, :iduff_invalido_menor)}
    #   let(:iduff_maior){create(:usuario, :iduff_invalido_maior)}

    #   it "Usuario invalido com iduff acima 11" do
    #       expect(iduff_maior).not_to be_valid
    #   end

    #   it "Usuario invalido com iduff abaixo de 11" do
    #     expect(iduff_menor).not_to be_valid
    #   end
    # end  
  end

  describe "creator#nome" do
    context "Presença de nome" do

      it "Usuario invalido com presença de nome" do
        creator = Pub::IdentificacaoLogin.new(nome: nil, iduff: "14454925712")
        creator.save(validate: false)
        usuario_invalido = Usuario.create(admin: true, creator: creator)
        expect(usuario_invalido).not_to be_valid
      end

      it "Usuario deve ser valido com nome presente" do
        expect(usuario).to be_valid
      end
    end
  end
  
end
