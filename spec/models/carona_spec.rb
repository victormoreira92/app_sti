require 'rails_helper'

RSpec.describe Carona, type: :model do
  let(:campus){create(:campu)}
  let(:carona2){create(:carona,partida:campus.nome)}
  let(:parada1){create(:parada, carona_id:campus.id, paradas: :ruadocarmo)}
  let(:parada2){create(:parada, carona_id:campus.id, paradas: :ruadorezende)}


  describe "#usuario" do
    context "Presença de usuario" do
      let(:campus){create(:campu)}
      let(:carona){create(:carona,partida:campus.nome)}
      let(:usuario){create(:usuario2)}

      
      it "Carona invalida sem usuario" do
        carona.usuario = nil
        expect(carona).not_to be_valid
      end

      it "Carona valida com Usuario" do
        carona.usuario = usuario
        expect(carona).to be_valid
      end

      it "Carona com usuario_id correto" do
        usuario_id = usuario.id
        carona.usuario = usuario
        expect(usuario_id).to eq(carona.usuario.id)
      end
    end
  end
  
  describe "#valor" do
    context "Valor em decimal" do
      let(:campus){create(:campu)}
      let(:carona){create(:carona,partida:campus.nome)}

      it "Valor de carona deve ser em decimal" do
        carona.valor = 8.45
        expect(carona.valor).to eq(BigDecimal('0.845e1')) 
      end
    end
    
    context "Valor minimo e maximo dos valores" do
      let(:campus){create(:campu)}
      let(:carona){create(:carona,partida:campus.nome)}
      
      it "Valor valido de carona deve está entre 0.00 e 999.99" do
        carona.valor = 999.90
        expect(carona).to be_valid 
      end

      it "Valor invalido de carona abaixo de 0.00" do
        carona.valor = -1
        expect(carona).not_to be_valid 
      end

      it "Valor invalido de carona acima de 999.99" do
        carona.valor = 1000.90
        expect(carona).not_to be_valid 
      end
    end
  end
  
  describe "#observacao" do
    context "Numero de caracteres" do
      it "Carona valida com observacao vazia" do
        carona2.observacao = ""
        expect(carona2).to be_valid 
      end
    end
it "observacao deve possuir no maximo 255" do
      carona2.observacao = Faker::Lorem.paragraph(sentence_count: 150)
      expect(carona2).not_to be_valid
    end
    
  end
  
  describe "#partida/#destino" do
     context "Presença de destino ou partida" do
       it "Carona invalida sem destino" do
        carona2.destino = nil
         expect(carona2).not_to be_valid
       end

       it "Carona invalida sem partida" do
        carona2.partida = nil
         expect(carona2).not_to be_valid
       end

       it "Carona invalida sem partida e destino" do
        carona2.partida = nil
        carona2.destino = nil
        expect(carona2).not_to be_valid
       end
     end
     
     context "Tamanho de caractres de partida" do
       it "Carona invalida se partida ter no menos de 3 caracteres " do
        carona2.partida = "as"
        expect(carona2).not_to  be_valid
       end

       it "Carona invalida se partida ter mais de 55" do
        carona2.partida = Faker::Lorem.paragraph(sentence_count: 56)
        expect(carona2).not_to  be_valid
      end

      it "Carona valida com partida maior que 3 e menor que 55 caracteres" do
        carona2.partida = campus.nome
        expect(carona2).to  be_valid

      end
      
       
       
     end
     
     context "Tamanho de caractres de destino" do
        it "Carona invalida se destino ter no menos de 3 caracteres " do
          carona2.destino = "as"
          expect(carona2).not_to  be_valid
        end

        it "Carona invalida se destino ter mais de 55" do
          carona2.destino = Faker::Lorem.paragraph(sentence_count: 56)
          expect(carona2).not_to  be_valid
        end

        it "Carona valida com destino maior que 3 e menor que 55 caracteres" do
          carona2.destino = campus.nome
          expect(carona2).to  be_valid
        end
      end

    end   
  
  describe "#passageiros" do
      context "numero de passageiros" do
        it "Carona valida com numero de pasageiros menor 5" do
          carona2.passageiros = 2
          expect(carona2).to  be_valid
        end

        it "Carona invalida com numero de pasageiros maior 5" do
          carona2.passageiros = 6
          expect(carona2).not_to  be_valid
        end

        it "Carona invalida com numero de pasageiros negativo" do
          carona2.passageiros = -6
          expect(carona2).not_to  be_valid
        end
      end
    end
    
  describe "#parada" do
    context "Presença de parada" do
      it "Carona valida sem parada" do
        carona2.paradas.each{|parada| parada = nil}
        expect(carona2).to be_valid 
      end
    end
    
  end
  
  
end
