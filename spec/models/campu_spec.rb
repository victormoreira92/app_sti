require 'rails_helper'

RSpec.describe Campu, type: :model do
  describe "#nome" do

    context "Tamanho do nome" do
      let(:campus){create(:campu)}

      it "nome valido com tamnho entre que 7 e 25" do
        campus.nome = "Campus Graogatá UFF"
        expect(campus).to be_valid
      end

      it "invalido nome menor que 5" do
        campus.nome = "campus"
        expect(campus).not_to be_valid
      end

      it "invalido nome maior que 25" do
        campus.nome = "Campus Graogatá - campus da UFF em Niteró Rio de Janeiro"
        expect(campus).not_to be_valid
      end
    end
  end

  describe "#endereco" do
    context "Tamanho" do
      let(:campus){create(:campu)}

      it "enedereco valido tamnho entre 15 e 25" do
        campus.endereco = "Rua do rezende, 115"
        expect(campus).to be_valid
      end

      it "invalido enedereco menor que 15" do
        campus.endereco = Faker::Lorem.sentence(word_count: 1)
        expect(campus).to be_valid
      end

      it "invalido endereco maior que 25" do
        campus.endereco = "Rua Tonelerios, s/n, lote 15, quadra 101, sudbida da rua do pedra negra e esquina com aa Francisco Bicalho"
        expect(campus).not_to be_valid
      end
    end
  end
  
  describe "#cep" do
    context "Tamanho do cep" do
      let(:campus){create(:campu)}

      it "Valido para ceps de 8" do
        campus.cep = "25580659"
        expect(campus).to be_valid
      end

      it "Invalido para ceps abaixo de 8" do
        campus.cep = "255806"
        expect(campus).not_to be_valid
      end

      it "Invalido para ceps acima de 8" do
        campus.cep = "255806455689"
        expect(campus).not_to be_valid
      end
      
    end
  end
  
  describe "#bairro" do
    context "Tamnho de caracteres de bairro" do
      let(:campus){create(:campu)}

      it "deve ser maior que 2 carcteres" do
        campus.bairro = "a"
        expect(campus).not_to be_valid
      end
    end
  end

  describe "#cidade" do
    context "Tamanho dos caracteres de cidade" do
      let(:campus){create(:campu)}

      it "deve ser maior que 2" do
        campus.cidade = "a"
        expect(campus).not_to be_valid
      end
    end
  end
  
  
end
