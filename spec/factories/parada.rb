FactoryBot.define do
    factory :parada do
        paradas {Faker::Address.street_name}

    end

    trait :ruadocarmo do 
        paradas {"Rua do carmo"}
    end

    trait :ruadorezende do 
        paradas {"Rua do rezende"}
    end
end 