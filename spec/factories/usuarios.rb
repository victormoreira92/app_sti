FactoryBot.define do
    factory :usuario do
      admin { false }
      data_desativacao  { Time.now }
      association :creator, :skip_validate
    end

    factory :usuario2, class: Usuario do
      admin { true }
      data_desativacao  { Time.now }
      association :creator,:creator2, :skip_validate
    end

    trait :admin do 
        admin {true}
    end

    trait :data_desativacao do 
      data_desativacao {nil}
    end

    trait :iduff_invalido_menor do
      association :creator, :iduff_com_numero_menor, :skip_validate
    end

    trait :iduff_invalido_maior do 
      association :creator, :iduff_com_numero_maior, :skip_validate
    end
  end 