FactoryBot.define do
    factory :campu do
        nome {"Campus Niteroi"}
        endereco {"Rua do matoso, lote 3"}
        numero {15}
        bairro {Faker::Address.city}
        cidade {Faker::Address.city}
        cep {"25580570"}
    end

    trait :nome_pequeno do 
        nome {Faker::Lorem.words(number: 1)}
    end

    trait :nome_grande do 
        nome {Faker::Lorem.words(number: 30)}
    end
end