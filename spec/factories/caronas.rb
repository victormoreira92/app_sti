FactoryBot.define do
    factory :carona do
        quando {Time.now}
        valor {8.45}
        passageiros {3}
        observacao {Faker::Lorem.sentence(word_count: 15)}
        partida {:campu}
        destino {Faker::Address.community}
        association :usuario
    end

   
end