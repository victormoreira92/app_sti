FactoryBot.define do
    factory :creator, class: Pub::IdentificacaoLogin do
        nome { "Daniel Pinheiro da Silva Junior" }
        iduff  { "13009660766" }
        ativo {"\x01"}

        
    end

    trait :creator2 do
        nome { "André Rezende dos Anjos" }
        iduff  { "14454925711" }
        ativo {"\x01"}
    end

    trait :skip_validate do
        to_create {|instance| instance.save(validate: false)}
    end

    trait :iduff_com_numero_menor do
        iduff {Faker::Number.number(digits: 9)}
    end

    trait :iduff_com_numero_maior do
        iduff {Faker::Number.number(digits: 13)}
    end

end